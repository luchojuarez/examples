/*
** Modulo que implementa la rutina de inicializacion.
** Inicializa el arreglo pasado como parametro con
** numeros aleatorios con tando 0, 100.
*/

/*
* Pre = {min_num<max_num}
* Pos = {Return random number n => min_num < n < max_num }
*/
int random_number(int min_num, int max_num){
	int result=0,low_num=0,hi_num=0;
	if(min_num<max_num){
		low_num=min_num;
		hi_num=max_num+1;
	}else{
		low_num=max_num+1;
		hi_num=min_num;
	}
	srand(time(NULL));
	result = (rand()%(hi_num-low_num))+low_num;
	return result;
}

/*
* Pre = {#array = max}
* Pos = {for all n:0<n<max: <Exists m :0<m<100: array[n]=m >}
*/
void init (int array[],int max){
	int i;
	for (i = 0; i < max; ++i){
		array[i]=random_number(i,100);
	}
}