#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include "init.c"

#define numNotas 6

int promedio (int notas[],int size){
	int i ,acum=0;
	for (i = 0; i < numNotas; ++i){
		acum += notas[i];
	}
	return (acum/size);
}


main(){
	int i, notas[numNotas];

	init (notas,numNotas); // Inicializo el arreglo con valores aleatorios

	int prom = promedio(notas,numNotas);
	
	for (i = 0; i < numNotas; ++i) {
		printf("%i ", notas[i]);
	}
	
	printf("\n%s %i \n","Promedio:",prom);
}