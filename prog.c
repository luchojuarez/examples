/*
** Dadas las notas de un alumno se quiere
** calcular el promedio de las notas por materia
*/
#include <stdio.h>

int promedio(){
	
	int nota1 = 9;
	int nota2 = 10;
	int nota3 = 7;
	int nota4 = 8;
	int nota5 = 9;
	int nota6 = 8;
	int nota7 = 3;
	int nota8 = 7;

	printf("\n%i %i %i %i %i %i %i %i\n",nota1, nota2, nota3, nota4, nota5, nota6, nota7, nota8);

	return (nota1 + nota2 + nota3 + nota4 + nota5 + nota6 + nota7 + nota8)/8;
}


main() {
	printf("promedio: %i \n",promedio() );
}